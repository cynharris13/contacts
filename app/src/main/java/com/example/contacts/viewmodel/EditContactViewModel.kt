package com.example.contacts.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.contacts.model.ContactRepo
import com.example.contacts.model.entity.Address
import com.example.contacts.model.entity.Contact
import com.example.contacts.view.editcontact.EditState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Edit contact view model.
 *
 * @property repo
 * @constructor Create empty Edit contact view model
 */
@HiltViewModel
class EditContactViewModel @Inject constructor(val repo: ContactRepo) : ViewModel() {
    private val _contact = MutableStateFlow(EditState())
    val contactState: StateFlow<EditState> get() = _contact

    /**
     * Get contact.
     *
     * @param id
     */
    fun getContact(id: Int) = viewModelScope.launch {
        _contact.update { it.copy(isLoading = true) }
        val contact = repo.getContact(id)
        _contact.update {
            it.copy(isLoading = false, contact = contact.contact, address = contact.address)
        }
    }

    /**
     * Update contact.
     *
     * @param contact
     */
    fun updateContact(contact: Contact) = viewModelScope.launch {
        repo.updateContact(contact)
    }

    /**
     * Update address.
     *
     * @param address
     */
    fun updateAddress(address: Address) = viewModelScope.launch {
        repo.updateAddress(address)
    }
}
