package com.example.contacts.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.contacts.model.ContactRepo
import com.example.contacts.model.entity.Address
import com.example.contacts.model.entity.Contact
import com.example.contacts.view.addcontact.NewContactState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * New contact view model.
 *
 * @property repo
 * @constructor Create empty New contact view model
 */
@HiltViewModel
class NewContactViewModel @Inject constructor(private val repo: ContactRepo) : ViewModel() {
    private val _newContact = MutableStateFlow(NewContactState())
    val newContactState: StateFlow<NewContactState> get() = _newContact

    /**
     * Add contact.
     *
     * @param contact
     */
    fun addContact(contact: Contact) = viewModelScope.launch {
        repo.addContact(contact)
    }

    /**
     * Add address.
     *
     * @param address
     */
    fun addAddress(address: Address) = viewModelScope.launch {
        repo.addAddress(address)
    }

    /**
     * Get address list size.
     *
     */
    fun getAddressListSize() = viewModelScope.launch {
        _newContact.update { it.copy(isLoading = true) }
        val size = repo.getAddressList().size
        _newContact.update { it.copy(isLoading = false, addressSize = size) }
    }
}
