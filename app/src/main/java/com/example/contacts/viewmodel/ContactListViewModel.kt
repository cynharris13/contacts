package com.example.contacts.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.contacts.model.ContactRepo
import com.example.contacts.view.contactlist.ContactListState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Contact list view model.
 *
 * @property repo
 * @constructor Create empty Contact list view model
 */
@HiltViewModel
class ContactListViewModel @Inject constructor(val repo: ContactRepo) : ViewModel() {
    private val _contacts = MutableStateFlow(ContactListState())
    val contactState: StateFlow<ContactListState> get() = _contacts

    /**
     * Get contact list.
     *
     */
    fun getContactList() = viewModelScope.launch {
        _contacts.update { it.copy(isLoading = true) }
        val contacts = repo.getContacts()
        _contacts.update { it.copy(isLoading = false, contacts = contacts) }
    }
}
