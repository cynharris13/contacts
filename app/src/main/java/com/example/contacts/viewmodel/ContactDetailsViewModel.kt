package com.example.contacts.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.contacts.model.ContactRepo
import com.example.contacts.view.contactdetails.ContactState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Contact details view model.
 *
 * @property repo
 * @constructor Create empty Contact details view model
 */
@HiltViewModel
class ContactDetailsViewModel @Inject constructor(val repo: ContactRepo) : ViewModel() {
    private val _contact = MutableStateFlow(ContactState())
    val contactState: StateFlow<ContactState> get() = _contact

    /**
     * Get contact.
     *
     * @param id
     */
    fun getContact(id: Int) = viewModelScope.launch {
        _contact.update { it.copy(isLoading = true) }
        val contact = repo.getContact(id)
        _contact.update { it.copy(isLoading = false, contact = contact.contact, address = contact.address) }
    }
}
