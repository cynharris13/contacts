package com.example.contacts.view.addcontact

/**
 * New contact state.
 *
 * @property isLoading
 * @property addressSize
 * @constructor Create empty New contact state
 */
data class NewContactState(
    val isLoading: Boolean = false,
    val addressSize: Int = 0
)
