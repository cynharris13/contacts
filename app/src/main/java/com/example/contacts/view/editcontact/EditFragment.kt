package com.example.contacts.view.editcontact

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.example.contacts.model.entity.Address
import com.example.contacts.model.entity.Contact
import com.example.contacts.ui.theme.ContactsTheme
import com.example.contacts.view.contactdetails.ContactDetailsFragmentArgs
import com.example.contacts.view.util.DisplayTextField
import com.example.contacts.viewmodel.EditContactViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass.
 * Use the [EditFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class EditFragment : Fragment() {
    private val args by navArgs<ContactDetailsFragmentArgs>()
    private val editContactViewModel by viewModels<EditContactViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val contactId = args.contactID
        // Inflate the layout for this fragment
        return ComposeView(requireActivity()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state by editContactViewModel.contactState.collectAsState()
                editContactViewModel.getContact(contactId)
                ContactsTheme() {
                    Surface(modifier = Modifier.fillMaxSize(), color = Color.Black) {
                        if (state.contact == null || state.address == null) { CircularProgressIndicator() } else {
                            val fName = remember { mutableStateOf(state.contact?.fName ?: "") }
                            val lName = remember { mutableStateOf(state.contact?.lName ?: "") }
                            val streetAddress = remember { mutableStateOf(state.address?.streetAddress ?: "") }
                            val city = remember { mutableStateOf(state.address?.city ?: "") }
                            val usState = remember { mutableStateOf(state.address?.state ?: "") }
                            val zipCode = remember { mutableStateOf(state.address?.zipcode ?: "") }
                            val phone = remember { mutableStateOf(state.contact?.phoneList ?: "") }
                            val email = remember { mutableStateOf(state.contact?.emailList ?: "") }
                            Column() {
                                DisplayTextField(fName, "First Name")
                                DisplayTextField(lName, "Last Name")
                                DisplayTextField(streetAddress, "Street Address")
                                DisplayTextField(city, "City")
                                DisplayTextField(usState, "State")
                                DisplayTextField(zipCode, "Zip Code")
                                DisplayTextField(phone, "Phone")
                                DisplayTextField(email, "Email")
                                Button(
                                    onClick = {
                                        val newAddress = Address(
                                            state.address?.id ?: 0,
                                            streetAddress.value,
                                            city.value,
                                            usState.value,
                                            zipCode.value
                                        )
                                        val newContact = Contact(
                                            state.contact!!.id,
                                            fName.value,
                                            lName.value,
                                            state.address?.id ?: 0,
                                            phone.value,
                                            email.value
                                        )
                                        editContactViewModel.updateAddress(newAddress)
                                        editContactViewModel.updateContact(newContact)
                                        findNavController().navigateUp()
                                    }
                                ) { Text(text = "Save new Contact") }
                            }
                        }
                    }
                }
            }
        }
    }
}
