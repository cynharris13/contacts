package com.example.contacts.view.util

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.graphics.Color

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DisplayTextField(remembered: MutableState<String>, label: String) {
    OutlinedTextField(
        value = remembered.value,
        onValueChange = { newText -> remembered.value = newText },
        label = { Text(label) },
        colors = TextFieldDefaults.textFieldColors(textColor = Color.Black)
    )
}
