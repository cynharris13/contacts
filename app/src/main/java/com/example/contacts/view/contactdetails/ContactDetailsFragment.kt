package com.example.contacts.view.contactdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.example.contacts.model.entity.Address
import com.example.contacts.model.entity.Contact
import com.example.contacts.ui.theme.ContactsTheme
import com.example.contacts.viewmodel.ContactDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass.
 * Use the [ContactDetailsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class ContactDetailsFragment : Fragment() {
    private val args by navArgs<ContactDetailsFragmentArgs>()
    private val contactDetailsViewModel by viewModels<ContactDetailsViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val contactId = args.contactID
        // Inflate the layout for this fragment
        return ComposeView(requireActivity()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state by contactDetailsViewModel.contactState.collectAsState()
                contactDetailsViewModel.getContact(contactId)
                ContactsTheme() {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = Color.Black
                    ) {
                        if (state.contact == null || state.address == null) {
                            CircularProgressIndicator()
                        } else {
                            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                                DisplayDetails(state.contact!!, state.address!!)
                                Button(
                                    onClick = {
                                        val direction = ContactDetailsFragmentDirections
                                            .actionContactDetailsFragmentToEditFragment(contactId)
                                        findNavController().navigate(direction)
                                    }
                                ) { Text(text = "Edit") }
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun DisplayDetails(contact: Contact, address: Address) {
    Text(text = "${contact.fName} ${contact.lName}", color = Color.White)
    Text(text = "${contact.phoneList}", color = Color.White)
    Text(text = "${contact.emailList}", color = Color.White)
    Text(text = "${address.streetAddress}", color = Color.White)
    Text(text = "${address.city}, ${address.state} ${address.zipcode}", color = Color.White)
}
