package com.example.contacts.view.editcontact

import com.example.contacts.model.entity.Address
import com.example.contacts.model.entity.Contact

/**
 * Edit state.
 *
 * @property isLoading
 * @property contact
 * @property address
 * @constructor Create empty Edit state
 */
data class EditState(
    val isLoading: Boolean = false,
    val contact: Contact? = null,
    val address: Address? = null
)
