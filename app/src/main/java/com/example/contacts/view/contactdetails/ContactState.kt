package com.example.contacts.view.contactdetails

import com.example.contacts.model.entity.Address
import com.example.contacts.model.entity.Contact

/**
 * Contact state.
 *
 * @property isLoading
 * @property contact
 * @property address
 * @constructor Create empty Contact state
 */
data class ContactState(
    val isLoading: Boolean = false,
    val contact: Contact? = null,
    val address: Address? = null
)
