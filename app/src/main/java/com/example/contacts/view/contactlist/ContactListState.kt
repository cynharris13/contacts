package com.example.contacts.view.contactlist

import com.example.contacts.model.entity.Contact

/**
 * Contact list state.
 *
 * @property contacts
 * @property isLoading
 * @property error
 * @constructor Create empty Contact list state
 */
data class ContactListState(
    val contacts: List<Contact> = emptyList(),
    val isLoading: Boolean = false,
    val error: String = ""
)
