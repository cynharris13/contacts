package com.example.contacts.view.contactlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.example.contacts.model.entity.Contact
import com.example.contacts.ui.theme.ContactsTheme
import com.example.contacts.viewmodel.ContactListViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass.
 * Use the [ContactListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class ContactListFragment : Fragment() {
    private val contactListViewModel by viewModels<ContactListViewModel>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // contactListViewModel.addContact(Contact(0, "fname", "lname", "address", "phone", "email"))
        // Inflate the layout for this fragment
        return ComposeView(requireActivity()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state by contactListViewModel.contactState.collectAsState()
                contactListViewModel.getContactList()
                ContactsTheme() {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = Color.Black
                    ) {
                        if (state.isLoading) { CircularProgressIndicator() }
                        Column() {
                            Button(
                                onClick = {
                                    val direction = ContactListFragmentDirections
                                        .actionContactListFragmentToAddContactFragment(
                                            state.contacts.size
                                        )
                                    findNavController().navigate(direction)
                                }
                            ) { Text(text = "Add Contact") }
                            LazyColumn {
                                var alphabet = ""
                                items(state.contacts) { contact ->
                                    if (contact.lName?.take(1) != alphabet) {
                                        alphabet = contact.lName?.take(1) ?: ""
                                        Text(
                                            text = alphabet.uppercase(),
                                            color = Color.Gray,
                                            modifier = Modifier.padding(10.dp)
                                        )
                                    }
                                    DisplayContact(contact) {
                                        val direction = ContactListFragmentDirections
                                            .actionContactListFragmentToContactDetailsFragment(contact.id)
                                        findNavController().navigate(direction)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    companion object {
        const val EIGHT = 8f
    }
}

@Composable
fun DisplayContact(contact: Contact, onclick: () -> Unit) {
    val fInit = contact.fName?.take(1) ?: ""
    val lInit = contact.lName?.take(1) ?: ""
    Button(
        onClick = { onclick() },
        colors = ButtonDefaults.buttonColors(containerColor = Color.DarkGray)
    ) {
        Row(modifier = Modifier.fillMaxWidth().height(30.dp)) {
            Box(
                modifier = Modifier
                    .border(5.dp, shape = CircleShape, color = Color.Green)
                    .background(Color.DarkGray)
                    .size(40.dp)
                    .fillMaxHeight()
            ) {
                Text(
                    text = fInit + lInit,
                    modifier = Modifier.padding(start = 12.dp, top = 5.dp)
                )
            }
            Text(
                text = "${contact.fName} ${contact.lName}",
                modifier = Modifier.padding(start = 30.dp, top = 5.dp)
            )
        }
    }
}
