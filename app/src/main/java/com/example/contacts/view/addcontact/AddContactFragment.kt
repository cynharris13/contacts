package com.example.contacts.view.addcontact

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.example.contacts.model.entity.Address
import com.example.contacts.model.entity.Contact
import com.example.contacts.ui.theme.ContactsTheme
import com.example.contacts.view.util.DisplayTextField
import com.example.contacts.viewmodel.NewContactViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Add contact fragment.
 *
 * @constructor Create empty Add contact fragment
 */
@AndroidEntryPoint
class AddContactFragment : Fragment() {
    private val newContactViewModel by viewModels<NewContactViewModel>()
    private val args by navArgs<AddContactFragmentArgs>()

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val size = args.currentSize
        return ComposeView(requireActivity()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state by newContactViewModel.newContactState.collectAsState()
                newContactViewModel.getAddressListSize()
                ContactsTheme() {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = Color.Black
                    ) {
                        Column() {
                            DisplayNewContactForm { fName, lName, streetAddress, city, usState, zipCode, phone, email ->
                                val newAddress = Address(state.addressSize, streetAddress, city, usState, zipCode)
                                val newContact = Contact(size, fName, lName, state.addressSize, phone, email)
                                newContactViewModel.addAddress(newAddress)
                                newContactViewModel.addContact(newContact)
                                findNavController().navigateUp()
                            }
                        }
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DisplayNewContactForm(onclick: (String, String, String, String, String, String, String, String) -> Unit) {
    val fName = remember { mutableStateOf("") }
    val lName = remember { mutableStateOf("") }
    val streetAddress = remember { mutableStateOf("") }
    val city = remember { mutableStateOf("") }
    val usState = remember { mutableStateOf("") }
    val zipCode = remember { mutableStateOf("") }
    val phone = remember { mutableStateOf("") }
    val email = remember { mutableStateOf("") }

    DisplayTextField(fName, "First Name")
    DisplayTextField(lName, "Last Name")
    DisplayTextField(streetAddress, "Street Address")
    DisplayTextField(city, "City")
    DisplayTextField(usState, "State")
    DisplayTextField(zipCode, "Zip Code")
    DisplayTextField(phone, "Phone")
    DisplayTextField(email, "Email")

    Button(
        onClick = {
            onclick(
                fName.value,
                lName.value,
                streetAddress.value,
                city.value,
                usState.value,
                zipCode.value,
                phone.value,
                email.value
            )
        }
    ) { Text(text = "Save new Contact") }
}
