package com.example.contacts

import android.app.Application
import android.content.Context
import dagger.hilt.android.HiltAndroidApp

/**
 * Contact application.
 *
 * @constructor Create empty Contact application
 */
@HiltAndroidApp
class ContactApplication : Application() {
    init {
        instance = this
    }
    companion object {
        private var instance: ContactApplication? = null

        /**
         * App context.
         *
         * @return
         */
        fun appContext(): Context {
            return instance!!.applicationContext
        }
    }
}
