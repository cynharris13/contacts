package com.example.contacts.model.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import com.example.contacts.model.entity.Contact
import com.example.contacts.model.entity.ContactAndAddress

/**
 * Contact d b.
 *
 * @constructor Create empty Contact d b
 */
@Dao
interface ContactDAO {
    @Query("select * from contacts")
    suspend fun getContacts(): List<Contact>

    @Transaction
    @Query("SELECT * FROM contacts WHERE contacts.id LIKE :id LIMIT 1")
    suspend fun findByID(id: Int): ContactAndAddress

    @Insert
    suspend fun insertAll(vararg contacts: Contact)

    /* unused
    @Delete
    suspend fun delete(contact: Contact)
     */

    @Update
    suspend fun updateContacts(vararg contact: Contact)
}
