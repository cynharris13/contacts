package com.example.contacts.model.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.contacts.model.entity.Address
import com.example.contacts.model.entity.Contact
import com.example.contacts.model.local.dao.AddressDAO
import com.example.contacts.model.local.dao.ContactDAO

@Database(
    entities = [Contact::class, Address::class],
    version = 1
)
abstract class ContactDB : RoomDatabase() {
    abstract fun contactDAO(): ContactDAO
    abstract fun addressDAO(): AddressDAO
}
