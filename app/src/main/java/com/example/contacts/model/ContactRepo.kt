package com.example.contacts.model

import com.example.contacts.model.entity.Address
import com.example.contacts.model.entity.Contact
import com.example.contacts.model.entity.ContactAndAddress
import com.example.contacts.model.local.ContactDB
import javax.inject.Inject

/**
 * Contact repo.
 *
 * @constructor Create empty Contact repo
 */
class ContactRepo @Inject constructor(private val contactDB: ContactDB) {
    private val contactDao = contactDB.contactDAO()
    private val addressDao = contactDB.addressDAO()

    /**
     * Get contacts.
     *
     * @return
     */
    suspend fun getContacts(): List<Contact> {
        return contactDao.getContacts().sortedBy { it.lName }
    }

    /**
     * Add contact.
     *
     * @param contact
     */
    suspend fun addContact(contact: Contact) {
        contactDao.insertAll(contact)
    }

    /**
     * Get contact.
     *
     * @param id
     * @return
     */
    suspend fun getContact(id: Int): ContactAndAddress {
        return contactDao.findByID(id)
    }

    /**
     * Update contact.
     *
     * @param contact
     */
    suspend fun updateContact(contact: Contact) {
        contactDao.updateContacts(contact)
    }

    /**
     * Add address.
     *
     * @param address
     */
    suspend fun addAddress(address: Address) {
        addressDao.insertAll(address)
    }

    /**
     * Get address list.
     *
     * @return
     */
    suspend fun getAddressList(): List<Address> {
        return addressDao.getAddressList()
    }

    /**
     * Update address.
     *
     * @param address
     */
    suspend fun updateAddress(address: Address) {
        addressDao.updateAddress(address)
    }
}
