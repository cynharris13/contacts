package com.example.contacts.model.di

import androidx.room.Room
import com.example.contacts.ContactApplication
import com.example.contacts.model.local.ContactDB
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object HiltModule {
    @Provides
    fun providesContactDB(): ContactDB {
        return Room.databaseBuilder(
            ContactApplication.appContext(),
            ContactDB::class.java,
            "contact-database"
        ).build()
    }
}
