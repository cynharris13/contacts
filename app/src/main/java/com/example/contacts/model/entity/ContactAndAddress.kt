package com.example.contacts.model.entity

import androidx.room.Embedded
import androidx.room.Relation

/**
 * Contact and address.
 *
 * @property contact
 * @property address
 * @constructor Create empty Contact and address
 */
data class ContactAndAddress(
    @Embedded val contact: Contact,
    @Relation(
        parentColumn = "address",
        entityColumn = "id"
    )
    val address: Address
)
