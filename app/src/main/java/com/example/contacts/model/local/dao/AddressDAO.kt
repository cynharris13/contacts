package com.example.contacts.model.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.contacts.model.entity.Address

@Dao
interface AddressDAO {
    @Query("select * from addresses")
    suspend fun getAddressList(): List<Address>

    /* unused
    @Query("SELECT * FROM addresses WHERE id LIKE :id LIMIT 1")
    suspend fun findByID(id: Int): Address
     */

    @Insert
    suspend fun insertAll(vararg address: Address)

    /* unused
    @Delete
    suspend fun delete(address: Address)
     */

    @Update
    suspend fun updateAddress(vararg address: Address)
}
