package com.example.contacts.model.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "contacts")
data class Contact(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "first_name") val fName: String?,
    @ColumnInfo(name = "last_name") val lName: String?,
    @ColumnInfo(name = "address") val addressID: Int?,
    @ColumnInfo(name = "phone_list") val phoneList: String?,
    @ColumnInfo(name = "email_list") val emailList: String?
)
