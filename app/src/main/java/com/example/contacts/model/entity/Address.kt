package com.example.contacts.model.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "addresses")
data class Address(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "street_address") val streetAddress: String?,
    val city: String?,
    val state: String?,
    val zipcode: String?
)
