package com.example.contacts.viewmodel

import com.example.contacts.model.ContactRepo
import com.example.contacts.model.entity.Address
import com.example.contacts.model.entity.Contact
import com.example.contacts.model.entity.ContactAndAddress
import com.example.contacts.util.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
internal class EditContactViewModelTest {
    @RegisterExtension
    val extension = CoroutinesTestExtension()
    private val repo = mockk<ContactRepo>()
    private val editVM = EditContactViewModel(repo)

    @Test
    @DisplayName("Tests getting contact and address")
    fun testGetContactAndAddress() = runTest(extension.dispatcher) {
        // given
        val expected = ContactAndAddress(
            Contact(0, "", "", 0, "", ""),
            Address(0, "", "", "", "")
        )
        coEvery { repo.getContact(0) } coAnswers { expected }

        // when
        editVM.getContact(0)

        // then
        Assertions.assertFalse(editVM.contactState.value.isLoading)
        Assertions.assertEquals(expected.contact, editVM.contactState.value.contact)
        Assertions.assertEquals(expected.address, editVM.contactState.value.address)
    }
}
