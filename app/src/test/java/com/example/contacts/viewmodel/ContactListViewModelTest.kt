package com.example.contacts.viewmodel

import com.example.contacts.model.ContactRepo
import com.example.contacts.model.entity.Contact
import com.example.contacts.util.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
internal class ContactListViewModelTest {
    @RegisterExtension
    val extension = CoroutinesTestExtension()
    private val repo = mockk<ContactRepo>()
    private val listVM = ContactListViewModel(repo)

    @Test
    @DisplayName("Tests getting the list of contacts")
    fun testGetContactList() = runTest(extension.dispatcher) {
        // given
        val expected = listOf(Contact(0, "", "", 0, "", ""))
        coEvery { repo.getContacts() } coAnswers { expected }

        // when
        listVM.getContactList()

        // then
        Assertions.assertFalse(listVM.contactState.value.isLoading)
        Assertions.assertEquals(expected, listVM.contactState.value.contacts)
    }
}
