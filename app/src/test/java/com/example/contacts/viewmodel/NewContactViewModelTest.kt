package com.example.contacts.viewmodel

import com.example.contacts.model.ContactRepo
import com.example.contacts.util.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
internal class NewContactViewModelTest {
    @RegisterExtension
    val extension = CoroutinesTestExtension()
    private val repo = mockk<ContactRepo>()
    private val newContactViewModel = NewContactViewModel(repo)

    @Test
    @DisplayName("Tests getting address list size")
    fun testGetAddressListSize() = runTest(extension.dispatcher) {
        // given
        val expected = 4
        coEvery { repo.getAddressList().size } coAnswers { expected }

        // when
        newContactViewModel.getAddressListSize()

        // then
        Assertions.assertFalse(newContactViewModel.newContactState.value.isLoading)
        Assertions.assertEquals(expected, newContactViewModel.newContactState.value.addressSize)
    }
}
