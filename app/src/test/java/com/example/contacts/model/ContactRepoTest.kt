package com.example.contacts.model

import com.example.contacts.model.entity.Address
import com.example.contacts.model.entity.Contact
import com.example.contacts.model.entity.ContactAndAddress
import com.example.contacts.model.local.ContactDB
import com.example.contacts.model.local.dao.AddressDAO
import com.example.contacts.model.local.dao.ContactDAO
import com.example.contacts.util.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
internal class ContactRepoTest {
    @RegisterExtension
    val extension = CoroutinesTestExtension()
    private val contactDB = mockk<ContactDB>()
    private val conDAO = mockk<ContactDAO>()
    private val addDAO = mockk<AddressDAO>()

    @BeforeEach
    fun setup() {
        coEvery { contactDB.contactDAO() } coAnswers { conDAO }
        coEvery { contactDB.addressDAO() } coAnswers { addDAO }
    }

    @Test
    @DisplayName("Tests retrieving the contact list")
    fun testGetContacts() = runTest(extension.dispatcher) {
        // given
        val repo = ContactRepo(contactDB)
        val expected = listOf(Contact(0, "", "", 0, "", ""))
        coEvery { conDAO.getContacts() } coAnswers { expected }

        // when
        val actual = repo.getContacts()

        // then
        Assertions.assertEquals(expected, actual)
    }

    @Test
    @DisplayName("Tests retrieving a contact based on its id")
    fun testGetContact() = runTest(extension.dispatcher) {
        // given
        val repo = ContactRepo(contactDB)
        val expected = ContactAndAddress(
            Contact(0, "", "", 0, "", ""),
            Address(0, "", "", "", "")
        )
        coEvery { conDAO.findByID(0) } coAnswers { expected }

        // when
        val actual = repo.getContact(0)

        // then
        Assertions.assertEquals(expected, actual)
    }

    @Test
    @DisplayName("Tests retrieving the address list")
    fun testGetAddresses() = runTest(extension.dispatcher) {
        // given
        val repo = ContactRepo(contactDB)
        val expected = listOf(Address(0, "", "", "", ""))
        coEvery { addDAO.getAddressList() } coAnswers { expected }

        // when
        val actual = repo.getAddressList()

        // then
        Assertions.assertEquals(expected, actual)
    }
}
